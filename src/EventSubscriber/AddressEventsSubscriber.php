<?php

namespace Drupal\address_iran\EventSubscriber;

use Drupal\address\Event\AddressEvents;
use Drupal\address\Event\AddressFormatEvent;
use Drupal\address\Event\SubdivisionsEvent;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use CommerceGuys\Addressing\AddressFormat\AddressField;
use CommerceGuys\Addressing\AddressFormat\AdministrativeAreaType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
/**
 * Adds a city field and a predefined list of province for Iran.
 *
 */
class AddressEventsSubscriber implements EventSubscriberInterface {
	
	use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AddressEvents::ADDRESS_FORMAT][] = ['onAddressFormat'];
    $events[AddressEvents::SUBDIVISIONS][] = ['onSubdivisions'];
    return $events;
  }

  /**
   * Alters the address format for Iran.
   *
   * @param \Drupal\address\Event\AddressFormatEvent $event
   *   The address format event.
   */
  public function onAddressFormat(AddressFormatEvent $event) {
    $definition = $event->getDefinition();
	
	if (isset($definition['country_code']) && $definition['country_code'] == 'IR') {
      $definition['format'] = "%organization\n%givenName %familyName\n%administrativeArea-%locality\n%postalCode\n%addressLine1\n%addressLine2";
      $definition['subdivision_depth'] = 2;
      $definition['required_fields'][] = AddressField::ADMINISTRATIVE_AREA;
      $definition['required_fields'][] = AddressField::LOCALITY;
      $definition['administrative_area_type'] = AdministrativeAreaType::PROVINCE;
      $event->setDefinition($definition);
    }
  }

  /**
   * Provides the subdivisions for Iran.
   *
   * @param \Drupal\address\Event\SubdivisionsEvent $event
   *   The subdivisions event.
   */
  public function onSubdivisions(SubdivisionsEvent $event) {
    $parents = $event->getParents();

    if ($parents == ['IR']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
            'آذربایجان شرقی'  => [
            'iso_code' => 'IR-AS',
            'has_children' => TRUE,
          ],
          'آذربایجان غربی' => [
            'iso_code' => 'IR-AG',
            'has_children' => TRUE,
          ],
          'اردبیل' => [
            'iso_code' => 'IR_AR',
            'has_children' => TRUE,
          ],
          'اصفهان' => [
            'iso_code' => 'IR_IS',
            'has_children' => TRUE,
          ],
          'البرز' => [
            'iso_code' => 'IR_AL',
            'has_children' => TRUE,
          ],
          'ایلام' => [
            'iso_code' => 'IR_IL',
            'has_children' => TRUE,
          ],
          'بوشهر' => [
            'iso_code' => 'IR_BO',
            'has_children' => TRUE,
          ],
          'تهران' => [
            'iso_code' => 'ID-TE',
            'has_children' => TRUE,
          ],
          'چهارمحال و بختیاری' => [
            'iso_code' => 'IR_CB',
            'has_children' => TRUE,
          ],
          'خراسان جنوبی' => [
            'iso_code' => 'IR_KJ',
            'has_children' => TRUE,
          ],
          'خراسان رضوی' => [
            'iso_code' => 'IR_KR',
            'has_children' => TRUE,
          ],
          'خراسان شمالی' => [
            'iso_code' => 'IR_KS',
            'has_children' => TRUE,
          ],
          'خوزستان' => [
            'iso_code' => 'IR_KH',
            'has_children' => TRUE,
          ],
          'زنجان' => [
            'iso_code' => 'IR_ZA',
            'has_children' => TRUE,
          ],
          'سمنان' => [
            'iso_code' => 'IR_SE',
            'has_children' => TRUE,
          ],
          'سیستان و بلوچستان' => [
            'iso_code' => 'IR_SB',
            'has_children' => TRUE,
          ],
          'فارس' => [
            'iso_code' => 'IR_FA',
            'has_children' => TRUE,
          ],
          'قزوین' => [
            'iso_code' => 'IR_QA',
            'has_children' => TRUE,
          ],
          'قم' => [
            'iso_code' => 'IR_QO',
            'has_children' => TRUE,
          ],
          'کردستان' => [
            'iso_code' => 'IR_KO',
            'has_children' => TRUE,
          ],
          'کرمان' => [
            'iso_code' => 'IR_KE',
            'has_children' => TRUE,
          ],
          'کرمانشاه' => [
            'iso_code' => 'IR_KN',
            'has_children' => TRUE,
          ],
          'کهگیلویه و بویراحمد' => [
            'iso_code' => 'IR_KB',
            'has_children' => TRUE,
          ],
          'گلستان' => [
            'iso_code' => 'IR_GO',
            'has_children' => TRUE,
          ],
          'گیلان' => [
            'iso_code' => 'IR_GI',
            'has_children' => TRUE,
          ],
          'لرستان' => [
            'iso_code' => 'IR_LO',
            'has_children' => TRUE,
          ],
          'مازندران' => [
            'iso_code' => 'IR_MA',
            'has_children' => TRUE,
          ],
          'مرکزی' => [
            'iso_code' => 'IR_MZ',
            'has_children' => TRUE,
          ],
          'هرمزگان' => [
            'iso_code' => 'IR_HO',
            'has_children' => TRUE,
          ],
          'همدان' => [
            'iso_code' => 'IR_HA',
            'has_children' => TRUE,
          ],
          'یزد' => [
            'iso_code' => 'IR_YZ',
            'has_children' => TRUE,
          ],
        ],
      ];
      $event->setDefinitions($definitions);
    }

    if ($parents == ['IR', 'آذربایجان شرقی']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'آذرشهر' => [],
          'اهر' => [],
          'اسکو' => [],
          'بستان آباد' => [],
          'بناب' => [],
          'تبریز' => [],
          'جلفا' => [],
          'چاراویماق' => [],
          'خداآفرین' => [],
          'سراب' => [],
          'شبستر' => [],
          'عجب شیر' => [],
          'کلیبر' => [],
          'مراغه' => [],
          'مرند' => [],
          'ملکان' => [],
          'میانه' => [],
          'ورزقان' => [],
          'هریس' => [],
          'هشترود' => [],
          'هوراند' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'آذربایجان غربی']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'ارومیه' => [],
          'اشنویه' => [],
          'بوکان' => [],
          'پیرانشهر' => [],
          'تکاب' => [],
          'خوی' => [],
          'چالدران' => [],
          'چایپاره' => [],
          'شوط' => [],
          'پلدشت' => [],
          'سردشت' => [],
          'سلماس' => [],
          'شاهین‌دژ' => [],
          'ماکو' => [],
          'مهاباد' => [],
          'میاندوآب' => [],
          'نقده' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'اردبیل']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'اردبیل' => [],
          'مشگین‌شهر' => [],
          'خلخال' => [],
          'گرمی' => [],
          'پارس‌آباد' => [],
          'بیله‌سوار' => [],
          'کوثر' => [],
          'نمین' => [],
          'نیر' => [],
          'سرعین' => [],
          'اصلاندوز' => [],
          'انگوت' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'اصفهان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'آران و بیدگل' => [],
          'اردستان' => [],
          'اصفهان' => [],
          'برخوار' => [],
          'بویین و میاندشت' => [],
          'تیران و کرون' => [],
          'چادگان' => [],
          'خمینی‌شهر' => [],
          'خوانسار' => [],
          'خور و بیابانک' => [],
          'سمیرم' => [],
          'شاهین‌شهر و میمه' => [],
          'شهرضا' => [],
          'دهاقان' => [],
          'فریدن' => [],
          'فریدون‌شهر' => [],
          'فلاورجان' => [],
          'کاشان' => [],
          'گلپایگان' => [],
          'لنجان' => [],
          'مبارکه' => [],
          'نایین' => [],
          'نجف‌آباد' => [],
          'نطنز' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'البرز']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'کرج' => [],
          'ساوجبلاغ' => [],
          'نظرآباد' => [],
          'طالقان' => [],
          'اشتهارد' => [],
          'فردیس' => [],
          'چهارباغ' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'ایلام']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'ایلام' => [],
          'دهلران' => [],
          'چرداول' => [],
          'ایوان' => [],
          'آبدانان' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'بوشهر']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'بوشهر' => [],
          'تنگستان' => [],
          'جم' => [],
          'دشتستان' => [],
          'دشتی' => [],
          'دیر' => [],
          'دیلم' => [],
          'عسلویه' => [],
          'کنگان' => [],
          'گناوه' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'تهران']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'اسلامشهر' => [],
          'بهارستان' => [],
          'پاکدشت' => [],
          'پردیس' => [],
          'پیشوا' => [],
          'تهران' => [],
          'دماوند' => [],
          'رباط‌کریم' => [],
          'ری' => [],
          'شمیرانات' => [],
          'شهریار' => [],
          'قدس' => [],
          'قرچک' => [],
          'فیروزکوه' => [],
          'ملارد' => [],
          'ورامین' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'چهارمحال و بختیاری']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'شهرکرد' => [],
          'بروجن' => [],
          'لردگان' => [],
          'فارسان' => [],
          'اردل' => [],
          'کوهرنگ' => [],
          'کیار' => [],
          'بن' => [],
          'سامان' => [],
          'خانمیرزا' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'خراسان جنوبی']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'بیرجند' => [],
          '	فردوس' => [],
          'طبس' => [],
          'قائنات' => [],
          'نهبندان' => [],
          'سربیشه' => [],
          'درمیان' => [],
          'سرایان' => [],
          'بشرویه' => [],
          'زیرکوه' => [],
          'خوسف' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'خراسان رضوی']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'باخرز' => [],
          'بجستان' => [],
          'بردسکن' => [],
          'بینالود' => [],
          'تایباد' => [],
          'تربت جام' => [],
          'تربت حیدریه' => [],
          'جغتای' => [],
          'جوین' => [],
          'چناران' => [],
          'خلیل‌آباد' => [],
          'خواف' => [],
          'خوشاب' => [],
          'داورزن' => [],
          'درگز' => [],
          'رشتخوار' => [],
          'زاوه' => [],
          'زبرخان' => [],
          'سبزوار' => [],
          'سرخس' => [],
          'ششتمد' => [],
          'صالح‌آباد' => [],
          'فریمان' => [],
          'فیروزه' => [],
          'قوچان' => [],
          'کاشمر' => [],
          'کلات' => [],
          'کوهسرخ' => [],
          'گلبهار' => [],
          'گناباد' => [],
          'مشهد' => [],
          'مه‌ولات' => [],
          'نیشابور' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'خراسان شمالی']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'بجنورد' => [],
          'شیروان' => [],
          'اسفراین' => [],
          'مانه و سملقان' => [],
          'فاروج' => [],
          'جاجرم' => [],
          'گرمه' => [],
          'راز و جرگلان' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'خوزستان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'اهواز' => [],
          'بهبهان' => [],
          'خرمشهر' => [],
          'ایذه' => [],
          'آبادان' => [],
          'دشت آزادگان' => [],
          'دزفول' => [],
          'شوشتر' => [],
          'مسجدسلیمان' => [],
          'رامهرمز' => [],
          'بندر ماهشهر' => [],
          'شادگان' => [],
          'اندیمشک' => [],
          'شوش' => [],
          'باغ‌ملک' => [],
          'امیدیه' => [],
          'لالی' => [],
          'هندیجان' => [],
          'اندیکا' => [],
          'گتوند' => [],
          'رامشیر' => [],
          'هویزه' => [],
          'هفتکل' => [],
          'باوی' => [],
          'کارون' => [],
          'حمیدیه' => [],
          'آغاجاری' => [],
          'کرخه' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'زنجان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'ابهر' => [],
          'طارم' => [],
          'ماهنشان' => [],
          'زنجان' => [],
          'خدابنده' => [],
          'ایجرود' => [],
          'خرم دره' => [],
          'سلطانیه' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'سمنان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'سمنان' => [],
          'شاهرود' => [],
          'دامغان' => [],
          'گرمسار' => [],
          'مهدیشهر' => [],
          'سرخه' => [],
          'میامی' => [],
          'آرادان' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'سیستان و بلوچستان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'خاش' => [],
          'زاهدان' => [],
          'زابل' => [],
          'ایرانشهر' => [],
          'چابهار' => [],
          'سراوان' => [],
          'نیکشهر' => [],
          'راسک' => [],
          'کنارک' => [],
          'زهک' => [],
          'دلگان' => [],
          'سیب و سوران' => [],
          'هیرمند' => [],
          'مهرستان' => [],
          'میرجاوه' => [],
          'قصرقند' => [],
          'نیمروز' => [],
          'هامون' => [],
          'فنوج' => [],
          'بمپور' => [],
          'تفتان' => [],
          'دشتیاری' => [],
          'سرباز' => [],
          'گلشن' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'فارس']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'آباده' => [],
          'ارسنجان' => [],
          'استهبان' => [],
          'اقلید' => [],
          'اوز' => [],
          'بختگان' => [],
          'بوانات' => [],
          'بیضا' => [],
          'پاسارگاد' => [],
          'جهرم' => [],
          'خرامه' => [],
          'خرم‌بید' => [],
          'خفر' => [],
          'خنج' => [],
          'داراب' => [],
          'رستم' => [],
          'زرقان' => [],
          'زرین‌دشت' => [],
          'سپیدان' => [],
          'سرچهان' => [],
          'سروستان' => [],
          'شیراز' => [],
          'فراشبند' => [],
          'فسا' => [],
          'فیروزآباد' => [],
          'قیر و کارزین' => [],
          'کازرون' => [],
          'کوار' => [],
          'کوه‌چنار' => [],
          'گراش' => [],
          'لارستان' => [],
          'لامرد' => [],
          'مرودشت' => [],
          'ممسنی' => [],
          'مُهر' => [],
          'نی‌ریز' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'قزوین']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'قزوین' => [],
          'البرز' => [],
          'تاکستان' => [],
          'بوئین‌زهرا' => [],
          'آبیک' => [],
          'آوج' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'قم']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'جعفرآباد' => [],
          'قم' => [],
          'کهک' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'کردستان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'سنندج' => [],
          'سقز' => [],
          'مریوان' => [],
          'بانه' => [],
          'قروه' => [],
          'کامیاران' => [],
          'بیجار' => [],
          'دیواندره' => [],
          'دهگلان' => [],
          'سروآباد' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'کرمان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'کرمان' => [],
          'بم' => [],
          'سیرجان' => [],
          'رفسنجان' => [],
          'جیرفت' => [],
          'بافت' => [],
          'زرند' => [],
          'شهربابک' => [],
          'بردسیر' => [],
          'کهنوج' => [],
          'راور' => [],
          'منوجان' => [],
          'عنبرآباد' => [],
          'کوهبنان' => [],
          'رودبار جنوب' => [],
          'قلعه گنج' => [],
          'ریگان' => [],
          'فهرج' => [],
          'انار' => [],
          'رابر' => [],
          'نرماشیر' => [],
          'ارزوئیه' => [],
          'فاریاب' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'کرمانشاه']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'اسلام‌آباد غرب' => [],
          'پاوه' => [],
          'ثلاث باباجانی' => [],
          'جوانرود' => [],
          'دالاهو' => [],
          'روانسر' => [],
          'سرپل ذهاب' => [],
          'سنقر' => [],
          'صحنه' => [],
          'قصرشیرین' => [],
          'کرمانشاه' => [],
          'کنگاور' => [],
          'گیلانغرب' => [],
          'هرسین' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'کهگیلویه و بویراحمد']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'بویراحمد (یاسوج)' => [],
          'بهمئی (لیکک)' => [],
          'دنا (سی سخت)' => [],
          'کهگیلویه (دهدشت)' => [],
          'گچساران (دوگنبدان)' => [],
          'چرام ' => [],
          'باشت' => [],
          'لنده' => [],
          'مارگون' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'گلستان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'آزادشهر' => [],
          'آق‌قلا' => [],
          'بندر گز' => [],
          'بندر ترکمن' => [],
          'رامیان' => [],
          'علی‌آباد' => [],
          'کردکوی' => [],
          'کلاله' => [],
          'گالیکش' => [],
          'گرگان' => [],
          'گمیشان' => [],
          'گنبد کاووس' => [],
          'مراوه‌تپه' => [],
          'مینودشت' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'گیلان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'رشت' => [],
          'طوالش' => [],
          'لاهیجان' => [],
          'رودسر' => [],
          'لنگرود' => [],
          'بندر انزلی' => [],
          'صومعه‌سرا' => [],
          'آستانه اشرفیه' => [],
          'رودبار' => [],
          'فومن' => [],
          'آستارا' => [],
          'رضوانشهر' => [],
          'رودسر' => [],
          'خمام' => [],
          'شفت' => [],
          'ماسال' => [],
          'سیاهکل' => [],
          'املش' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'لرستان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'ازنا' => [],
          'الیگودرز' => [],
          'بروجرد' => [],
          'پل‌دختر' => [],
          'چگنی' => [],
          'خرم‌آباد' => [],
          'دلفان' => [],
          'دورود' => [],
          'رومشکان' => [],
          'سلسله' => [],
          'کوهدشت' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'مازندران']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'آمل' => [],
          'بابل' => [],
          'بابلسر' => [],
          'بهشهر' => [],
          'جویبار' => [],
          'چالوس' => [],
          'کلاردشت' => [],
          'رامسر' => [],
          'ساری' => [],
          'سوادکوه' => [],
          'سوادکوه شمالی' => [],
          'سیمرغ' => [],
          'تنکابن' => [],
          'عباس‌آباد' => [],
          'فریدون‌کنار' => [],
          'قائم‌شهر' => [],
          'گلوگاه' => [],
          'محمودآباد' => [],
          'میان‌دورود' => [],
          'نکا' => [],
          'نوشهر' => [],
          'نور' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'مرکزی']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'اراک' => [],
          'ساوه' => [],
          'خمین' => [],
          'محلات' => [],
          'دلیجان' => [],
          'شازند' => [],
          'زرندیه' => [],
          'تفرش' => [],
          'کمیجان' => [],
          'آشتیان' => [],
          'خنداب' => [],
          'فراهان' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'هرمزگان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'بندرعباس' => [],
          'بندرلنگه' => [],
          'میناب' => [],
          'قشم' => [],
          'ابوموسی' => [],
          'جاسک' => [],
          'رودان' => [],
          'حاجی‌آباد' => [],
          'بستک' => [],
          'پارسیان' => [],
          'خمیر' => [],
          'سیریک' => [],
          'بشاگرد' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'همدان']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'همدان' => [],
          'ملایر' => [],
          'نهاوند' => [],
          'تویسرکان' => [],
          'کبودرآهنگ' => [],
          'اسدآباد' => [],
          'بهار' => [],
          'رزن' => [],
          'فامنین' => [],
          'درگزین' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }
    if ($parents == ['IR', 'یزد']) {
      $definitions = [
        'country_code' => $parents[0],
        'parents' => $parents,
        'subdivisions' => [
          'ابرکوه' => [],
          'اردکان' => [],
          'اشکذر' => [],
          'بافق' => [],
          'بهاباد' => [],
          'تفت' => [],
          'خاتم' => [],
          'مهریز' => [],
          'میبد' => [],
          'یزد' => [],
        ],
      ];
      $event->setDefinitions($definitions);
    }

  }

}
